package com.zuitt.batch212;
import java.util.InputMismatchException;
import java.util.Scanner;

public class WDC043_S03_A1 {
    public static void main(String[] args) {

        Scanner appScanner = new Scanner(System.in);


//        System.out.println("\n" + "Input an Integer whose factorial will be computed:");
//        int num = appScanner.nextInt();
//
//        long factorial = 1;
//
//        if (num <= 0){
//            System.out.println("Number should be non negative or 0");
//        }
//        else {
//            for (int i = 1; i <= num; i++) {
//                factorial = (factorial * i);
//            }
//            System.out.println("The factorial of" + " " + num + " " + "is" + " " + factorial);
//        }




//        System.out.println("\n" + "Input an Integer whose factorial will be computed:");
//        int num2 = appScanner.nextInt();
//
        long factorial2 = 1;
//        int i = 1;
////
//        if (num2 <= 0){
//            System.out.println("Number should be non negative or 0");
//        }
//        else {
//            while (i <= num2) {
//                factorial2 = factorial2 * i;
//                i++;
//            }
//            System.out.println("The factorial of" + " " + num2 + " " + "is" + " " + factorial2);
//        }




        Scanner input = new Scanner(System.in);
        int num1 = 0;

        try {
            System.out.println("\n" + "Input an Integer whose factorial will be computed:");
            num1 = input.nextInt();
        }
        catch (InputMismatchException e){
            System.out.println("Input is not a number");
        }
        catch (Exception e){
            System.out.println("Invalid input");
        }
        finally {
            if (num1 <= 0){
                System.out.println("Number should be non negative or 0");
            }
            else {
                for (int i = 1; i <= num1; i++) {
                    factorial2 *= i;
                }
                System.out.println("The factorial of" + " " + num1 + " " + "is" + " " + factorial2);
            }

        }









    }
}
